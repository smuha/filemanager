import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

/**
 * Created by Admin on 29.01.14.
 */
@WebServlet("/back")
public class OneStepBack extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        HttpSession session = request.getSession();

        String currentPath = ((String) session.getAttribute("currentPath"));
        if (!currentPath.equals(session.getAttribute("rootPath"))) {
            currentPath = currentPath.substring(0, currentPath.length() - (new File(currentPath)).getName().length() - 1);
            session.setAttribute("currentPath", currentPath);
            String[] names = ContentScanner.getFoldersList((String) session.getAttribute("currentPath"));
            session.setAttribute("names", names);
        }

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/fileManager.jsp");
        dispatcher.forward(request, response);
    }
}
