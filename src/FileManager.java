import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by Admin on 24.01.14.
 */
@WebServlet("/fm")
public class FileManager extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        HttpSession session = request.getSession();

        if ((new File(session.getAttribute("currentPath") + request.getParameter("path"))).isFile()) {
            try {
                Desktop.getDesktop().open(new File(session.getAttribute("currentPath") + request.getParameter("path")));
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else {
            session.setAttribute("previousPath", session.getAttribute("currentPath"));
            session.setAttribute("currentPath", session.getAttribute("currentPath") + request.getParameter("path") + File.separator);
        }

        session.setAttribute("names", ContentScanner.getFoldersList((String) session.getAttribute("currentPath")));
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/fileManager.jsp");
        dispatcher.forward(request, response);
    }
}
