import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Admin on 29.01.14.
 */
@WebServlet("/RootPath")
public class CreateRootPath extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        HttpSession session = request.getSession();
        session.setAttribute("currentPath", request.getParameter("path"));
        session.setAttribute("rootPath", request.getParameter("path"));
        String[] names = ContentScanner.getFoldersList((String) session.getAttribute("currentPath"));
        session.setAttribute("names", names);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/fileManager.jsp");
        dispatcher.forward(request, response);

    }
}
