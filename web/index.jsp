<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
</head>
<body>
<div style="text-align: center;">
    <h1>Please, enter your root path:</h1>
    <h4>(For example, if you're Windows user - "D:\", or if you're Linux user - "/home")</h4>
    <form method="get" action="/RootPath" accept-charset="utf-8">
        <input name="path" type="text" size="20" value="D:\">
        <input type="submit" value="Go">
    </form>
    <br>P.S: I'm not sure, that manager will be working, if this app will be launched on Linux
</div>
</body>
</html>